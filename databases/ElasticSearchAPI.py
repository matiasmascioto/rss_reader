"""
ElasticSearch API
"""

import os
import yaml
import requests
from elasticsearch import Elasticsearch


class ElasticSearchAPI:

    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

    FILE_PATH = __location__

    config = yaml.load(open(os.path.join(FILE_PATH, "ElasticSearchAPI.yml"), encoding="utf-8"))

    def __init__(self):
        self.res = requests.get(self.config["db"]["http"])
        #print(self.res.content)

        self.es = Elasticsearch([{"host": self.config["db"]["host"], "port": self.config["db"]["port"]}])

    def insert_items(self, items):
        """ Inserts 'items' into ElasticSearch """
        # Save in Index (ElasticSearch). Index as ID
        for item in items:
            self.es.index(index='news', doc_type='rss', id=item['link']['text'], body={"object": item})
            # print(item['link']['text'])





