"""
MongoDB API
"""

import pymongo
from pymongo import MongoClient
from pymongo import UpdateOne
import os
import yaml


class MongoDBAPI:

    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

    FILE_PATH = __location__

    config = yaml.load(open(os.path.join(FILE_PATH, "MongoDBAPI.yml"), encoding="utf-8"))

    def __init__(self):
        self.host = self.config["db"]["host"]
        self.db = self.config["db"]["db"]
        self.collection = self.config["db"]["collection"]
        self.indexes = self.config["db"]["collection_indexes"]
        self.client = None

    def connect(self):
        """ Connects to MongoDB server and returns client """
        self.client = MongoClient(self.host)
        return self.client

    def setup(self):
        """ Creates self.collection with self.indexes if collection was not created yet """
        db = self.client[self.db]
        collections = db.collection_names()

        if self.collection not in collections: # Creates new collection
            coll = pymongo.collection.Collection(db, self.collection, create=True)

            for index in self.indexes:
                if index["type"] == "text":   # Text index
                    coll.create_index(index["index"], default_language=index["default_language"], unique=index["unique"])
                else:   # Normal index
                    coll.create_index(index["index"], unique=index["unique"])

    def insert_items(self, items):
        """ Inserts 'items' into MongoDB """
        coll = self.client[self.db][self.collection]

        requests = []
        for doc in items:
            update = UpdateOne({"link.text": doc["link"]["text"]}, {"$set":doc}, upsert=True)
            requests.append(update)

        try:
            response = coll.bulk_write(requests)
            return response.upserted_count
        except Exception as e:
            return 0

    def disconnect(self):
        """ Closes MongoClient instance """
        self.client.close()