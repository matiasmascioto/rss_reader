"""
XML Reader (RSS and ATOM)
"""

import xml.etree.ElementTree
from dateutil.parser import parse
import re
import requests


class XMLReader:

    def __init__(self, rss_link):
        self.link = rss_link

        try:
            xml_string = requests.get(rss_link).text    # Saves HTTP response as string
        except requests.exceptions.RequestException as e:
            print(e)
            xml_string = ""

        # Replaces dots in namespaces to avoid conflicts with MongoDB keys
        xml_string = re.sub(" xmlns:(\w)+=\"[^\"]+\"", lambda s: s.group(0).replace(".", "_"), xml_string)

        # Reads 'xml_string' as ElementTree
        try:
            self.root = xml.etree.ElementTree.fromstring(xml_string)
        except xml.etree.ElementTree.ParseError:
            self.root = None

    @staticmethod
    def __element_to_dict(element):
        """ Returns 'element' as dict """
        element_dict = dict()
        for child in element:
            element_dict[child.tag] = dict()
            element_dict[child.tag]["text"] = child.text
            for key, value in child.items():
                element_dict[child.tag][key] = value

        return element_dict

    @staticmethod
    def __pubdate_to_datetime(item):
        """ Converts 'pubDate' field to datetime (if exists) """
        if "pubDate" in item.keys():
            item["pubDate"]["datetime"] = parse(item["pubDate"]["text"])

    @staticmethod
    def __reformat_http(item):
        """ Reformats 'link.text' field to avoid duplicates """
        if "link" in item.keys():
            item["link"]["text"] = item["link"]["text"].replace("https", "http")

    def get_items(self, category = None, media = None):
        """ Returns 'items' as a list """
        items = list()

        if self.root is not None:
            for item in self.root.iter("item"):
                dict_item = XMLReader.__element_to_dict(item)
                XMLReader.__pubdate_to_datetime(dict_item)
                XMLReader.__reformat_http(dict_item)
                dict_item["category"] = category
                dict_item["media"] = media

                items.append(dict_item)

        return items

    def get_channel_data(self):
        """ Returns channel data as a dict """
        channel_data = dict()

        if self.root is not None:
            childs = list(self.root.find("channel"))
            childs_filtered = [child for child in childs if child.tag != "item"]
            channel_data = XMLReader.__element_to_dict(childs_filtered)

        return channel_data
