# Lector de Canales RSS

Lector de Canales RSS en Python. Guarda el contenido de los canales RSS indicados en MongoDB y/o ElasticSearch.

## Configuración
### General
Es posible configurar los motores de bases de datos a utilizar (MongoDB y/o ElasticSearch), como así también el período de consulta de los canales RSS en el archivo `config.yml`

### Canales RSS
Los canales RSS que se desean procesar y almacenar pueden incluirse en el archivo `rss_channels.yml`.

### Bases de datos
La configuración de MongoDB y ElasticSearch está en los archivos `MongoDBAPI.yml` y `ElasticSearchAPI.yml` respectivamente.


## Tecnologías

* [Python](https://www.python.org/) - Procesamiento de canales RSS
* [MongoDB](https://www.mongodb.com/) - Almacenamiento de noticias
* [ElasticSearch](https://www.elastic.co/) - Almacenamiento de noticias

## Autores

* **Franco Urbinati**
* **Juan Matías Mascioto**