"""
Main Project file
"""

import os
import yaml
import time
from XMLReader import XMLReader
from databases.MongoDBAPI import MongoDBAPI
from databases.ElasticSearchAPI import ElasticSearchAPI


PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))

channels_config = yaml.safe_load(open("rss_channels.yml", encoding="utf-8"))
config = yaml.safe_load(open("config.yml", encoding="utf-8"))


def main(dbs, channels, period):
    """ Main function """
    # MongoDB connection
    if dbs["mongodb"]:
        mongodb_api = MongoDBAPI()
        mongodb_api.connect()

        mongodb_api.setup()

    # ElasticSearch connection
    if dbs["elastic_search"]:
        es_api = ElasticSearchAPI()

    print("-- Iniciando Lector de Canales RSS -- ")
    print("MongoDB: {0} -- ElasticSearch: {1}".format(dbs["mongodb"], dbs["elastic_search"]))
    print("periodo: {0} segundos\n".format(period))

    try:
        while 1:
            for category_title, category_channels in channels.items():
                for media, channel_link in category_channels.items():
                    channel_reader = XMLReader(channel_link)
                    #channel_data = channel_reader.get_channel_data()
                    items = channel_reader.get_items(category_title, media)

                    if dbs["mongodb"]:
                        inserted_count = mongodb_api.insert_items(items)    # Saves 'items' in MongoDB

                    if dbs["elastic_search"]:
                        es_api.insert_items(items)  # Saves 'items' in ElasticSearch

                    if inserted_count > 0:
                        print("{0} - {1} - Noticias guardadas: {2}".format(category_title, media, inserted_count))

            print("Próxima iteración en {0} segundos\n".format(period))
            time.sleep(period)
    except KeyboardInterrupt:
        mongodb_api.disconnect()


if __name__ == "__main__":
    dbs = config["dbs"]
    period = config["period"]
    channels = channels_config["channels_by_category"]

    main(dbs, channels, period)